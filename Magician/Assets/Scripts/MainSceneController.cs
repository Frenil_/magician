﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Advertisements;

public class MainSceneController : MonoBehaviour, IUnityAdsListener
{
    public GameObject PrisonPopup;
    string gameid = "3173642";
    bool testmode = true;
    // Start is called before the first frame update
    void Start()
    {
        Advertisement.AddListener(this);
        Advertisement.Initialize(gameid, testmode);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void onBtnEntryToDungeon()
    {
        SceneManager.LoadScene("StageScene",LoadSceneMode.Single);
    }

    public void onBtnRewardedAds()
    {
        Advertisement.Show("rewardedVideo");
    }

    // Implement IUnityAdsListener interface methods:
    public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
    {
        // Define conditional logic for each ad completion status:
        if (showResult == ShowResult.Finished)
        {
            Debug.Log("UnityAds Finished!!!!!");
            // Reward the user for watching the ad to completion.
        }
        else if (showResult == ShowResult.Skipped)
        {
            Debug.Log("UnityAds Skiped!!!!!");
            // Do not reward the user for skipping the ad.
        }
        else if (showResult == ShowResult.Failed)
        {
            Debug.LogWarning("UnityAds Error!!!!!");
        }
    }

    public void OnUnityAdsReady(string placementId)
    {
        // If the ready Placement is rewarded, show the ad:
        if (placementId == "rewardedVideo")
        {
            Debug.Log("Rewarded Vidio Ready");
        }
    }

    public void OnUnityAdsDidError(string message)
    {
        // Log the error.
    }

    public void OnUnityAdsDidStart(string placementId)
    {
        // Optional actions to take when the end-users triggers an ad.
    }

    public void onBtnOpenPrison()
    {
        PrisonPopup.SetActive(!PrisonPopup.activeSelf);
    }
}
