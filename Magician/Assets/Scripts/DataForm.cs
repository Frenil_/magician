﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;
using System.Xml;
using System.Xml.Serialization;

public enum Status : byte {Non, Atk, Def, Hp, Spd, Spirit, Max_Spirit, Hel_Spirit, Dmg_Minus, Dmg_Plus};
public enum ItemClass : byte {Non, Normal, Expert, Master};
public enum StageType : byte {Non, Normal, Named, Boss};
public enum MoneyType : byte {Non, Gold, ADS};
public enum ItemType : byte {Non,Stone, Gold,Jewel, Neck, Bracelet, EarRing, Pocket };


public struct Buff_Data
{
    public byte Index;
    public byte[] Class;
    public Status[] Facters_Type;
    public byte[] Facter_Value;

}
public struct Element_Data
{
    public byte Element_ID;
    public sbyte Hp;//-128 ~ 127
    public sbyte Atk;
    public sbyte Def;
    public sbyte Spd;
}
public struct Equipment_Data//보류
{
    public byte Equip_ID;
    public byte Item_Class;
    public Status[] Base_Option;
    public sbyte[] Base_Value;
}
public struct EXP_Data
{
    public byte Lv;
    public uint Max_Exp;
}
public struct Familiar_Data
{
    public uint Monster_ID;
    public uint String_ID;
    public string Code;
    public string Character_path;
    public string Icon_Path;
    public byte Class_ID;
    public byte Force_ID;//힐,탱,딜...
    public byte Element_ID;
    public byte Spd;
    public byte[] Skill_ID;
    /*
     * skill, script 추가예정
     */
}
public struct Familiar_Book_Data
{
    public byte Book_Index;
    public uint Familiar_ID;
    public string Path;
    public bool Active_F;
    public uint[] Familiar_Level_ID;
}
public struct Familiar_Script_Data
{
    public byte Familiar_Script_ID;
    public string Kr;
    public string En;
    public string Jp;
}
public struct Force_Data
{
    public byte Force_ID;
    public sbyte Base_Hp;
    public sbyte Base_Atk;
    public sbyte Base_def;
    public sbyte Hp_Lv;
    public sbyte Atk_Lv;
    public sbyte Def_Lv;
    public sbyte Hp_Class;
    public sbyte Atk_Class;
    public sbyte Def_Class;
    public sbyte Hp_Vari;
    public sbyte Atk_Vari;
    public sbyte Def_Vari;
    public sbyte Spd;
}
public struct Inven_Data
{
    public byte Inven_ID;
    public uint item_ID;
    public uint Item_EA;
    public Status[] Rand_Options;
    public uint[] Rand_Value;
}
public struct Item_Data
{
    public uint Item_ID;
    public byte String_ID;
    public ItemType Item_Type;
    public uint Item_buy_Price;
    public uint Item_Sell_Price;
    public uint Item_Stack_Limit;
}
public struct Item_Option_Data
{
    public byte Item_Class;
    public Status Item_Rand_Option;
    public float[] Value;
    public float Rate;
}
public struct Monster_Data
{
    public uint Monster_ID;
    public uint String_ID;
    public string Code;
    public string Character_path;
    public string Icon_Path;
    public byte Class_ID;
    public byte Force_ID;//힐,탱,딜...
    public byte Element_ID;
    public byte Spd;
    public int[] Skill_ID;  
}
public struct Prototype_Shop_Data
{
    public byte Shop_ID;
    public string Item;
    public uint Item_Ea;
    public uint Price;
    public MoneyType Buy_Type;
}
public struct Stage_Data {
    public byte Chapter_No;
    public byte Stage_No;
    public byte Round_No;
    public StageType Monster_Type;
    public uint[] Monster_ID;
    public byte[] Monster_Lv;
    public byte[] VariType;
}
[XmlRoot("Prison_Familiar_Data")]
public class Prison_Familiar_Data
{
    [XmlElement("familiarID")]
    public int familiarID=0;
    [XmlElement("isGet")]
    public bool isGet=false;
    [XmlElement("now_like")]
    public int now_like=0;
    [XmlElement("max_like")]
    public int max_like=0;
}

public struct String_Script_Data
{
    public uint String_ID;
    public string Kr;
    public string En;
    public string Jp;
}


public class XMLLoader
{
    Status StringToStatus(string s)
    {
        switch (s)
        {
            case "Atk":
                return Status.Atk;
            case "Def":
                return Status.Def;
            case "Hp":
                return Status.Hp;
            case "Spd":
                return Status.Spd;
            case "Spirit":
                return Status.Spirit;
            case "Dmg_Minus":
                return Status.Dmg_Minus;
            case "Dmg_Plus":
                return Status.Dmg_Plus;
            default:
                return Status.Non;
        }
    }
    ItemClass StringToItemClass(string s)
    {
        if (s == "Normal") return ItemClass.Normal;
        else if (s == "Expert") return ItemClass.Expert;
        else if (s == "Master") return ItemClass.Master;
        return ItemClass.Non;
    }
    StageType StringToStageType(string s) {
        if (s == "Normal") return StageType.Normal;
        else if (s == "Expert") return StageType.Named;
        else if (s == "Master") return StageType.Boss;
        return StageType.Non;
    }
    MoneyType StringToMoneyType(string s)
    {
        if (s == "Gold") return MoneyType.Gold;
        else if (s == "ADS") return MoneyType.ADS;
        return MoneyType.Non;
    }
    ItemType StringToItemType(string s)
    {
        switch (s)
        {
            case "Atk":
                return ItemType.Stone;
            case "Def":
                return ItemType.Gold;
            case "Hp":
                return ItemType.Jewel;
            case "Spd":
                return ItemType.Neck;
            case "Spirit":
                return ItemType.Bracelet;
            case "Dmg_Minus":
                return ItemType.EarRing;
            case "Dmg_Plus":
                return ItemType.Pocket;
            default:
                return ItemType.Non;
        }
    }


    public List<Buff_Data> LoadBuff_Data(string path)
    {
        XmlDocument Doc = new XmlDocument();
        Doc.Load(path);
        XmlElement xmlElement = Doc.DocumentElement;

        List<Buff_Data> buff_list = new List<Buff_Data>();

        foreach (XmlElement E in xmlElement.ChildNodes)
        {
            Buff_Data temp = new Buff_Data();
            temp.Class = new byte[3];
            temp.Facters_Type = new Status[3];
            temp.Facter_Value = new byte[3];

            temp.Index = System.Convert.ToByte(E.GetAttribute("Index"));

            string strclass;
            string strtype;
            string strvalue;
            for(int i=0; i<3; ++i)
            {
                strclass = "Class_" + (i + 1).ToString();
                strtype = "Facter_" + (i + 1).ToString();
                strvalue = "Facter_" + (i + 1).ToString() + "_Value";
                temp.Class[i] = System.Convert.ToByte(E.GetAttribute(strclass));
                temp.Facters_Type[i] = StringToStatus(E.GetAttribute(strtype));
                string s = E.GetAttribute(strvalue);
                if (Status.Non != temp.Facters_Type[i])
                {
                    if (s != "")
                    {
                        temp.Facter_Value[i] = System.Convert.ToByte(E.GetAttribute(strvalue));
                    }
                }

            }
            Array.Sort(temp.Class);
            
            buff_list.Add(temp);
        }


        return buff_list;
    }
    public List<Element_Data> LoadElement_Data(string path)
    {
        XmlDocument Doc = new XmlDocument();
        Doc.Load(path);
        XmlElement xmlElement = Doc.DocumentElement;

        List<Element_Data> element_list = new List<Element_Data>();

        foreach (XmlElement E in xmlElement.ChildNodes)
        {
            Element_Data temp = new Element_Data();
            temp.Element_ID = System.Convert.ToByte(E.GetAttribute("Element_ID"));
            temp.Hp = System.Convert.ToSByte(E.GetAttribute("Hp"));
            temp.Atk = System.Convert.ToSByte(E.GetAttribute("Atk"));
            temp.Def = System.Convert.ToSByte(E.GetAttribute("Def"));
            temp.Spd = System.Convert.ToSByte(E.GetAttribute("Spd"));

            element_list.Add(temp);
        }

        return element_list;
    }
    public List<Equipment_Data> LoadEquipment_Data(string path)
    {
        XmlDocument Doc = new XmlDocument();
        Doc.Load(path);
        XmlElement xmlElement = Doc.DocumentElement;

        List<Equipment_Data> equipment_list = new List<Equipment_Data>();

        foreach (XmlElement E in xmlElement.ChildNodes)
        {
            Equipment_Data temp = new Equipment_Data();
            temp.Base_Option = new Status[3];
            temp.Base_Value = new sbyte[3];

            temp.Equip_ID = System.Convert.ToByte(E.GetAttribute("Equip_ID"));
            temp.Item_Class = System.Convert.ToByte(E.GetAttribute("Item_Class"));


            for(int i=0; i<3; ++i)
            {
                string basetype = "Item_Base_Option_type_" + (i + 1).ToString();
                string option = "Base_Value_" + (i + 1).ToString();

                temp.Base_Option[i] = StringToStatus(E.GetAttribute(basetype));
                if (E.GetAttribute(option) == "null"|| E.GetAttribute(option) == "")
                {
                    temp.Base_Value[i] = 0;
                }
                else
                {
                    temp.Base_Value[i] = System.Convert.ToSByte(E.GetAttribute(option));
                }
            }

            equipment_list.Add(temp);
        }

        return equipment_list;
    }
    public List<EXP_Data> LoadEXP_Data(string path)
    {
        XmlDocument Doc = new XmlDocument();
        Doc.Load(path);
        XmlElement xmlElement = Doc.DocumentElement;

        List<EXP_Data> exp_list = new List<EXP_Data>();
        foreach (XmlElement E in xmlElement.ChildNodes)
        {
            EXP_Data temp = new EXP_Data();
            temp.Lv = System.Convert.ToByte(E.GetAttribute("Lv"));
            temp.Max_Exp = System.Convert.ToUInt32(E.GetAttribute("Max_Exp"));
            exp_list.Add(temp);
        }

        return exp_list;
    }
    public List<Familiar_Data> LoadFamiliar_Data(string path)
    {
        XmlDocument Doc = new XmlDocument();
        Doc.Load(path);
        XmlElement xmlElement = Doc.DocumentElement;

        List<Familiar_Data> familiar_list = new List<Familiar_Data>();

        foreach (XmlElement E in xmlElement.ChildNodes)
        {
            Familiar_Data temp = new Familiar_Data();
            temp.Monster_ID = System.Convert.ToUInt32(E.GetAttribute("Monster_ID"));
            temp.String_ID = System.Convert.ToUInt32(E.GetAttribute("String_ID"));
            temp.Code = E.GetAttribute("Zomb");
            temp.Character_path = E.GetAttribute("Character_path");
            temp.Icon_Path = E.GetAttribute("Icon_Path");
            temp.Class_ID = System.Convert.ToByte(E.GetAttribute("Class_ID"));
            temp.Force_ID = System.Convert.ToByte(E.GetAttribute("Force_ID"));
            temp.Element_ID = System.Convert.ToByte(E.GetAttribute("Element_ID"));
            temp.Spd = System.Convert.ToByte(E.GetAttribute("Spd"));

            temp.Skill_ID = new byte[3];
            for (int i = 0; i < 3; ++i)
            {
                string skill = "Skill_ID_" + (i + 1).ToString();
                string s = E.GetAttribute(skill);
                if (s != "")
                {
                    temp.Skill_ID[i] = System.Convert.ToByte(E.GetAttribute(skill));
                }
            }
            familiar_list.Add(temp);
        }

        return familiar_list;
    }
    public List<Familiar_Book_Data> LoadFamiliar_Book_Data(string path)
    {
        XmlDocument Doc = new XmlDocument();
        Doc.Load(path);
        XmlElement xmlElement = Doc.DocumentElement;

        List<Familiar_Book_Data> familiar_book_list = new List<Familiar_Book_Data>();

        foreach (XmlElement E in xmlElement.ChildNodes)
        {
            Familiar_Book_Data temp = new Familiar_Book_Data();
            temp.Book_Index = System.Convert.ToByte(E.GetAttribute("Book_Index"));
            temp.Familiar_ID = System.Convert.ToUInt32(E.GetAttribute("Familiar_ID"));
            temp.Path = E.GetAttribute("Path");
            temp.Active_F = System.Convert.ToByte(E.GetAttribute("Active_F")) ==0 ? false : true;
            temp.Familiar_Level_ID = new uint[3];
            for (int i=0; i<3; ++i)
            {
                string id = "Familiar_ID_" + (i + 1).ToString();
                temp.Familiar_Level_ID[i] = System.Convert.ToUInt32(E.GetAttribute(id));
            }

            familiar_book_list.Add(temp);
        }

        return familiar_book_list;
    }
    public List<Familiar_Script_Data> LoadFamiliar_Script_Data(string path)
    {
        XmlDocument Doc = new XmlDocument();
        Doc.Load(path);
        XmlElement xmlElement = Doc.DocumentElement;

        List<Familiar_Script_Data> familiar_script_list = new List<Familiar_Script_Data>();

        foreach (XmlElement E in xmlElement.ChildNodes)
        {
            Familiar_Script_Data temp = new Familiar_Script_Data();
            temp.Familiar_Script_ID = System.Convert.ToByte(E.GetAttribute("Familiar_Script_ID"));
            temp.Kr = E.GetAttribute("Kr");
            temp.En = E.GetAttribute("En");
            temp.Jp = E.GetAttribute("Jp");


            familiar_script_list.Add(temp);
        }

        return familiar_script_list;
    }
    public List<Force_Data> LoadForce_Data(string path)
    {
        XmlDocument Doc = new XmlDocument();
        Doc.Load(path);
        XmlElement xmlElement = Doc.DocumentElement;

        List<Force_Data> force_list = new List<Force_Data>();

        foreach (XmlElement E in xmlElement.ChildNodes)
        {
            Force_Data temp = new Force_Data();
            temp.Force_ID = System.Convert.ToByte(E.GetAttribute("Force_ID"));
            temp.Base_Hp = System.Convert.ToSByte(E.GetAttribute("Base_Hp"));
            temp.Base_Atk = System.Convert.ToSByte(E.GetAttribute("Base_Atk"));
            temp.Base_def = System.Convert.ToSByte(E.GetAttribute("Base_def"));
            temp.Hp_Lv = System.Convert.ToSByte(E.GetAttribute("Hp_Lv"));
            temp.Atk_Lv = System.Convert.ToSByte(E.GetAttribute("Atk_Lv"));
            temp.Def_Lv = System.Convert.ToSByte(E.GetAttribute("Def_Lv"));
            temp.Hp_Class = System.Convert.ToSByte(E.GetAttribute("Hp_Class"));
            temp.Atk_Class = System.Convert.ToSByte(E.GetAttribute("Atk_Class"));
            temp.Def_Class = System.Convert.ToSByte(E.GetAttribute("Def_Class"));
            temp.Hp_Vari = System.Convert.ToSByte(E.GetAttribute("Hp_Vari"));
            temp.Atk_Vari = System.Convert.ToSByte(E.GetAttribute("Atk_Vari"));
            temp.Def_Vari = System.Convert.ToSByte(E.GetAttribute("Def_Vari"));
            temp.Spd = System.Convert.ToSByte(E.GetAttribute("Spd"));

            force_list.Add(temp);
        }

        return force_list;
    }
    public List<Item_Data> LoadItem_Data(string path)
    {
        XmlDocument Doc = new XmlDocument();
        Doc.Load(path);
        XmlElement xmlElement = Doc.DocumentElement;

        List<Item_Data> item_list = new List<Item_Data>();

        foreach (XmlElement E in xmlElement.ChildNodes)
        {
            Item_Data temp = new Item_Data();
            temp.Item_ID = System.Convert.ToUInt32(E.GetAttribute("Item_ID"));
            temp.String_ID = System.Convert.ToByte(E.GetAttribute("String_ID"));
            temp.Item_Type = StringToItemType(E.GetAttribute("Item_Type"));
            temp.Item_buy_Price = System.Convert.ToUInt32(E.GetAttribute("Item_buy_Price"));
            temp.Item_Sell_Price = System.Convert.ToUInt32(E.GetAttribute("Item_Sell_Price"));
            temp.Item_Stack_Limit = System.Convert.ToUInt32(E.GetAttribute("Item_Stack_Limit"));

            item_list.Add(temp);
        }

        return item_list;
    }
    public List<Item_Option_Data> LoadItem_Option_Data(string path)
    {
        XmlDocument Doc = new XmlDocument();
        Doc.Load(path);
        XmlElement xmlElement = Doc.DocumentElement;

        List<Item_Option_Data> item_list = new List<Item_Option_Data>();

        foreach (XmlElement E in xmlElement.ChildNodes)
        {
            Item_Option_Data temp = new Item_Option_Data();
            temp.Item_Class = System.Convert.ToByte(E.GetAttribute("Item_Class"));
            temp.Item_Rand_Option = StringToStatus(E.GetAttribute("Item_Rand_Option"));
            temp.Value = new float[2];
            for (int i = 0; i < 2; ++i)
            {
                string val = "Value_" + (i + 1).ToString();
                temp.Value[i] = System.Convert.ToSingle(E.GetAttribute(val));
            }
            temp.Rate = System.Convert.ToSingle(E.GetAttribute("rate"));

            item_list.Add(temp);
        }
        return item_list;
    }
    public List<Monster_Data> LoadMonster_Data(string path)
    {
        XmlDocument Doc = new XmlDocument();
        Doc.Load(path);
        XmlElement xmlElement = Doc.DocumentElement;

        List<Monster_Data> monster_list = new List<Monster_Data>();

        foreach (XmlElement E in xmlElement.ChildNodes)
        {
            Monster_Data temp = new Monster_Data();
            temp.Monster_ID = System.Convert.ToUInt32(E.GetAttribute("Monster_ID"));
            temp.String_ID = System.Convert.ToByte(E.GetAttribute("String_ID"));
            temp.Code = E.GetAttribute("Code");
            temp.Character_path = E.GetAttribute("Character_path");
            temp.Icon_Path = E.GetAttribute("Icon_Path");
            temp.Class_ID = System.Convert.ToByte(E.GetAttribute("Class_ID"));
            temp.Force_ID = System.Convert.ToByte(E.GetAttribute("Force_ID"));
            temp.Element_ID = System.Convert.ToByte(E.GetAttribute("Element_ID"));
            temp.Spd = System.Convert.ToByte(E.GetAttribute("Spd"));

            temp.Skill_ID = new int[6];
            for (int i = 0; i < 6; ++i)
            {
                string skill = "Skill_ID_" + (i + 1).ToString();
                if (E.GetAttribute(skill)!="")
                {
                    temp.Skill_ID[i] = System.Convert.ToInt32(E.GetAttribute(skill));
                }
                else
                {
                    temp.Skill_ID[i] = -1;
                }
            }
            monster_list.Add(temp);
        }

        return monster_list;
    }
    public List<Prototype_Shop_Data> LoadPrototype_Shop_Data(string path)
    {
        XmlDocument Doc = new XmlDocument();
        Doc.Load(path);
        XmlElement xmlElement = Doc.DocumentElement;

        List<Prototype_Shop_Data> shop_list = new List<Prototype_Shop_Data>();

        foreach (XmlElement E in xmlElement.ChildNodes)
        {
            Prototype_Shop_Data temp = new Prototype_Shop_Data();
            temp.Shop_ID = System.Convert.ToByte(E.GetAttribute("Shop_ID"));
            temp.Item = E.GetAttribute("Item");
            temp.Item_Ea = System.Convert.ToUInt32(E.GetAttribute("Item_Ea"));
            temp.Price = System.Convert.ToUInt32(E.GetAttribute("Price"));
            temp.Buy_Type = StringToMoneyType(E.GetAttribute("Buy_Type"));
            shop_list.Add(temp);
        }

        return shop_list;
    }
    public List<Stage_Data> LoadStage_Data(string path)
    {
        XmlDocument Doc = new XmlDocument();
        Doc.Load(path);
        XmlElement xmlElement = Doc.DocumentElement;

        List<Stage_Data> stage_list = new List<Stage_Data>();

        foreach (XmlElement E in xmlElement.ChildNodes)
        {
            Stage_Data temp = new Stage_Data();
            temp.Chapter_No = System.Convert.ToByte(E.GetAttribute("Chapter_No"));
            temp.Stage_No = System.Convert.ToByte(E.GetAttribute("Stage_No")); 
            temp.Round_No = System.Convert.ToByte(E.GetAttribute("Round_No"));
            temp.Monster_Type = StringToStageType(E.GetAttribute("Monster_Type"));

            temp.Monster_ID = new uint[5];
            temp.Monster_Lv = new byte[5];
            temp.VariType = new byte[5];

            for (int i=0; i<5; ++i)
            {
                string mon = "Monster_ID_" + (i+1).ToString();
                string lv = "Monster_Lv_" + (i+1).ToString();
                string type = "VariType_" + (i+1).ToString();
                if (E.GetAttribute(mon) != "")
                    temp.Monster_ID[i] = System.Convert.ToUInt32(E.GetAttribute(mon));
                if (E.GetAttribute(lv) != "")
                    temp.Monster_Lv[i] = System.Convert.ToByte(E.GetAttribute(lv));
                if (E.GetAttribute(type) != "")
                    temp.VariType[i] = System.Convert.ToByte(E.GetAttribute(type));
            }

            stage_list.Add(temp);
        }

        return stage_list;
    }



    public List<String_Script_Data> LoadString_Script_Data(string path)
    {
        XmlDocument Doc = new XmlDocument();
        Doc.Load(path);
        XmlElement xmlElement = Doc.DocumentElement;

        List<String_Script_Data> string_script_list = new List<String_Script_Data>();

        foreach (XmlElement E in xmlElement.ChildNodes)
        {
            String_Script_Data temp = new String_Script_Data();
            temp.String_ID = System.Convert.ToByte(E.GetAttribute("String_ID"));
            temp.Kr = E.GetAttribute("Kr");
            temp.En = E.GetAttribute("En");
            temp.Jp = E.GetAttribute("Jp");

            string_script_list.Add(temp);
        }

        return string_script_list;
    }
  



}