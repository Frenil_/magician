﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Element_Battle : MonoBehaviour
{
    public enum Element_State
    {
        Wait,
        Disable,//상급
        Set
    }
    Element_State state;
    Vector3 startpos;
    public Vector3 setpos;
    Element_Manager element_manager;
    Image image;
    byte element_type;
    byte trigger_count;

    protected sbyte set_index;

    // Start is called before the first frame update
    void Awake()
    {
        state = Element_State.Disable;
        startpos = transform.position;
        image = GetComponent<Image>();
        trigger_count = 0;
        set_index = -1;
    }
    public void SetManager(Element_Manager manager)
    {
        element_manager = manager;
    }
    public void ChangeElementType(byte type, Sprite sprite, bool special = true)
    {
        element_type = type;
        image.sprite = sprite;
        image.enabled = true;
        if (!special)
        {
            state = Element_State.Wait;
        }
    }

    public byte GetElementType()
    {
        return element_type;
    }
    public Vector3 GetStartPos()
    {
        return startpos;
    }
    public Vector3 GetSetPos()
    {
        return setpos;
    }
    public Element_State GetState()
    {
        return state;
    }
    public sbyte GetSettingIndex()
    {
        return set_index;
    }

    public void SetSettingIndex(sbyte index)
    {
        set_index = index;
    }

    public void DropElement()
    {
        if (state == Element_State.Wait)
        {
            if (setpos != startpos && trigger_count > 0)
            {
                transform.position = setpos;
                element_manager.AddSetElement(this, setpos);
            }
            else
            {
                element_manager.SubSetElement(this);
                ResetPosition();
            }
        }
    }
    public void ResetPosition()
    {
        transform.position = startpos;
        setpos = startpos;
        state = Element_State.Wait;
    }
    //상급 전용
    public void SetDisable()
    {
        state = Element_State.Disable;
        image.enabled = false;
    }


    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Element_Set" && state == Element_State.Wait)
        {
            setpos = other.transform.position;
            ++trigger_count;
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Element_Set" && state == Element_State.Wait)
        {
            --trigger_count;
            if (trigger_count < 1)
            {
                setpos = startpos;
            }
        }
    }
}
