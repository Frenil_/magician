﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Familiar_Battle : BattleObject
{
    public void SetIconSprite(Sprite sp)
    {
        transform.Find("Image").GetComponent<Image>().sprite = sp;
    }
}
