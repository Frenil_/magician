﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Element_Manager : MonoBehaviour
{
    Atlas_Manager atlas_manager;
    Element_Battle[] normal_elements;
    Element_Battle special_element;
    bool using_special = false;

    Sprite[] element_sprites;
    public List<Element_Battle> setting_element;

    Vector3[] setting_pos;
    public byte[] setting_type;

    byte[] familiar_element;
    // Start is called before the first frame update
    void Start()
    {
        atlas_manager = GameObject.Find("AtlasManager").GetComponent<Atlas_Manager>();
        element_sprites = atlas_manager.GetElementSprite();

        setting_element = new List<Element_Battle>();
        normal_elements = new Element_Battle[8];
        setting_pos = new Vector3[3];
        setting_type = new byte[3];
        familiar_element = new byte[3];

        familiar_element[0] = 101;
        familiar_element[1] = 102;
        familiar_element[2] = 103;

        for (int i = 0; i < 3; ++i)
        {
            //스킬 세팅 좌표지정
            string name = "element_Set_" + i.ToString();
            setting_pos[i] = GameObject.Find(name).transform.position;
            setting_type[i] = System.Convert.ToByte(0);
            //familiar 타입 데이터 로딩 추가
        }

        special_element = GameObject.Find("element_0").GetComponent<Element_Battle>();
        special_element.SetManager(this);

        for (int i = 0; i < normal_elements.Length; ++i)
        {
            string name = "element_" + (i + 1).ToString();
            normal_elements[i] = GameObject.Find(name).GetComponent<Element_Battle>();
            normal_elements[i].SetManager(this);
            int n = Random.Range(0, 3);
            normal_elements[i].ChangeElementType(familiar_element[n], element_sprites[familiar_element[n] - 101], false);
        }

    }

    private void ResetElements(byte special = 0)
    {
        foreach (var element in normal_elements)
        {
            int n = Random.Range(0, 3);
            element.ChangeElementType(familiar_element[n], element_sprites[familiar_element[n] - 101], false);
            element.ResetPosition();
        }
        if (!using_special)
        {
            special_element.SetDisable();
        }
        else if(special!=0)
        {
            ActiveSpecialElement(special);
        }
        special_element.ResetPosition();

        for (int i = 0; i < 3; ++i)
        {
            setting_type[i] = 0;
        }
        setting_element.Clear();
    }


    private Element_Battle SettingElementByPosition(Vector3 pos)
    {

        foreach (var element in setting_element)
        {
            if (element.transform.position == pos) { return element; }
        }
        return null;
    }

    private void CastSkill(byte special)
    {
        byte[] setting_count;
        byte skilltype = 0;//0 - 1중첩(실패), 1 - 2중첩(2스킬), 2 - 3중첩(3스킬) 
        bool special_skill = false;//false - 2스킬 or 실패, true - 3스킬
        byte casting_element = 0; //스킬 속성 0 - 실패, 101 - 빛....
        setting_count = new byte[3];

        for (int i = 0; i < setting_count.Length; ++i)//for문 빠른 탈출 고려해볼것
        {
            for (int j = 0; j < familiar_element.Length; ++j)
            {
                if (familiar_element[j] == setting_type[i])
                {
                    ++setting_count[j];
                    if (setting_count[j] == 2)
                    {
                        skilltype = 1;
                        casting_element = familiar_element[j];
                    }
                    else if (setting_count[j] == 3)
                    {
                        skilltype = 2;
                        casting_element = familiar_element[j];
                    }
                }
            }
        }
        if (special != 0)
        {
            using_special = false;
        }
        if (special == casting_element)
        {
            special_skill = true;
        }


        if (skilltype == 2)
        {
            if (special_skill)//3스킬 3중첩
            {
                Debug.Log("3스킬 3중첩");
                ResetElements();
            }
            else//2스킬 3중첩
            {
                Debug.Log("2스킬 3중첩");
                using_special = true;
                ResetElements(casting_element);
            }
        }
        else if (skilltype == 1)
        {
            if (special_skill)//3스킬 2중첩
            {
                Debug.Log("3스킬 2중첩");
                ResetElements();
            }
            else//2스킬 2중첩
            {
                Debug.Log("2스킬 2중첩");
                ResetElements();
            }
        }
        else//실패
        {
            Debug.Log("실패");
            ResetElements();
        }

    }

    private void ActiveSpecialElement(byte type)
    {
        int n = type - 101 + 4;
        special_element.ChangeElementType(System.Convert.ToByte(type + 10), element_sprites[n]);
    }

    public void AddSetElement(Element_Battle element, Vector3 setpos)
    {
        //세팅된 element를 제자리 혹은 다른 자리로 넣을경우 값을 빼버리고 새로 계산
        for (int i=0; i< setting_element.Count; ++i)
        {
            if(setting_element[i] == element&&element.GetSettingIndex()!=-1)
            {
                setting_type[element.GetSettingIndex()] = 0;
                element.SetSettingIndex(-1);
            }
        }
        setting_element.Remove(element);


        for (int i = 0; i < setting_pos.Length; ++i)
        {
            if (setting_pos[i] == setpos)//3개의 세팅좌표중 일치하는곳 
            {
                if (setting_type[i] == 0)//세팅된 element가 없을때
                {
                    setting_element.Add(element);
                }
                else
                {
                    Element_Battle temp = SettingElementByPosition(setting_pos[i]);
                    if (temp && temp != element)//등록된 element와 추가할 element가 다를때
                    {
                        setting_element.Add(element);
                        setting_element.Remove(temp);
                        temp.ResetPosition();
                    }
                }
                setting_type[i] = element.GetElementType();
                element.SetSettingIndex(System.Convert.ToSByte(i));
                break;
            }
        }

        if (setting_element.Count == 3)
        {
            byte special = 0;
            //foreach(var set in setting_type)
            for (var i = 0; i < setting_type.Length; ++i)
            {
                if (setting_type[i] > 110)
                {
                    byte temp = System.Convert.ToByte(setting_type[i] - 10);
                    setting_type[i] = temp;
                    special = temp;
                }
            }
            CastSkill(special);
        }

    }
    public void SubSetElement(Element_Battle element)
    {
        for (int i = 0; i < setting_pos.Length; ++i)
        {
            if (setting_pos[i] == element.GetSetPos())
            {
                setting_type[i] = 0;
            }
        }
        element.ResetPosition();
        setting_element.Remove(element);
    }

}
