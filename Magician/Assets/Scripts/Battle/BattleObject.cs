﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleObject : MonoBehaviour
{
    protected int Max_Hp;
    protected byte Atk;
    protected byte Def;
    protected byte Spd;

    protected int Hp;
    protected float Cooltime;
    protected float Attacktimer;

    protected bool alive;

    Battle_Manager battle_manager;
    Image Hp_bar;

    void Awake()
    {
        Hp = Max_Hp = 0;
        Atk = 0;
        Def = 0;
        Spd = 0;
        Cooltime = 60f;
        Attacktimer = 0;
        alive = false;

        Hp_bar = transform.Find("HpBar").GetComponent<Image>();
    }

    public void SetStat(int _hp, byte _atk, byte _def, byte _spd)
    {
        Hp = Max_Hp = _hp;
        if (Hp > 0)
            alive = true;
        Atk = _atk;
        Def = _def;
        Spd = _spd;

    }

    public void SetBattleManager(Battle_Manager bm)
    {
        battle_manager = bm;
    }

    public virtual void SufferDamage(int damage)//공격 받음
    {
        Hp -= damage - Def;
        Hp_bar.fillAmount = Hp / (float)Max_Hp;
        if (Hp <= 0)
        {
            alive = false;
        }
    }

    public bool isAlive()
    {
        return alive;
    }

    private void FixedUpdate()
    {
        if (alive)
        {
            Attacktimer += Time.fixedDeltaTime * Spd;
            if (Attacktimer >= Cooltime)
            {
                Attacktimer -= Cooltime;
                if (battle_manager)
                {
                    //나중에 공격에관한 계산식이 개선되어야 할듯.
                    battle_manager.BasicAttack(this, Atk);
                }
            }
        }
    }

}
