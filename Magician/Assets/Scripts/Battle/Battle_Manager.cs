﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Battle_Manager : MonoBehaviour
{
    List<Enemy_Battle> Enemies;
    List<Familiar_Battle> Familiars;
    Sprite[] familiar_sprites;

    Element_Battle drag_element;

    byte Enemies_count;
    byte PlayerTargetIndex;



    // Start is called before the first frame update
    void Start()
    {
        PlayerTargetIndex = 77; //77은 아무것도 타게팅 안한 숫자

        Enemies_count = 3;

        familiar_sprites = new Sprite[3];

        familiar_sprites[0] = Resources.Load("Character/Icon/Icon_Darkelf4_0",typeof(Sprite)) as Sprite;
        familiar_sprites[1] = Resources.Load("Character/Icon/Icon_Druid4_0", typeof(Sprite)) as Sprite;
        familiar_sprites[2] = Resources.Load("Character/Icon/Icon_Lamia4_0", typeof(Sprite)) as Sprite;

        Enemies = new List<Enemy_Battle>();
        Familiars = new List<Familiar_Battle>();
        GameObject monster_position = GameObject.Find("MonsterPosition");
        Transform monster_group = GameObject.Find("Monsters").transform;
        Transform familiar_group = GameObject.Find("Familiars").transform;

        for (int i = 0; i < Enemies_count; ++i)
        {
            GameObject temp = Instantiate(Resources.Load("Prefab/Battle/Enemy", typeof(GameObject)), monster_group) as GameObject;
            Transform pos = monster_position.transform.Find(Enemies_count.ToString() + "-" + (i + 1).ToString());
            temp.gameObject.transform.localPosition = pos.localPosition;

            var obje = temp.GetComponent<Enemy_Battle>();
            obje.SetStat(20, 2, 1, 60);
            obje.SetBattleManager(this);

            Enemies.Add(obje);
        }

        for (int i = 0; i < 3; ++i)
        {
            GameObject temp = Instantiate(Resources.Load("Prefab/Battle/Familiar", typeof(GameObject)), familiar_group) as GameObject;
            temp.gameObject.transform.localPosition = new Vector3(-200 + 200 * i, 100, 0);

            var obje = temp.GetComponent<Familiar_Battle>();
            obje.SetStat(20, 2, 1, 120);
            obje.SetBattleManager(this);
            obje.SetIconSprite(familiar_sprites[i]);

            Familiars.Add(obje);
        }

    }

    private void StageSetting()
    {
        DataManager data = GameObject.Find("DataManager").GetComponent<DataManager>();


    }


    public void BasicAttack(BattleObject requester, int damage)
    {

        if (requester.transform.tag == "Enemy")
        {
            //while (Familiars.Count > 0)
            {
                byte targetnumber = (byte)Random.Range(0, Familiars.Count);
                if (Familiars[targetnumber].isAlive())
                {
                    Familiars[targetnumber].SufferDamage(damage);
                    //effect
                    //break;
                }
            }
        }
        else if (requester.transform.tag == "Familiar")
        {
            //몹이 타겟팅 되었음
            if (PlayerTargetIndex != 77 && PlayerTargetIndex < Enemies.Count)
            {
                if (Enemies[PlayerTargetIndex].isAlive())
                {
                    Enemies[PlayerTargetIndex].SufferDamage(damage);
                    //effect
                }
            }
            //몹이 타겟팅 안됐음 랜덤어택
            else
            {
               // while (Enemies.Count > 0)
                {
                    byte targetnumber = (byte)Random.Range(0, Enemies.Count);
                    if (Enemies[targetnumber].isAlive())
                    {
                        Enemies[targetnumber].SufferDamage(damage);
                        //effect
                        //break;
                    }
                }
            }
        }
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector2 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D[] rays = Physics2D.RaycastAll(pos, Vector2.zero, 0f);

            foreach(var hit in rays)
            {
                if (hit.collider != null && hit.transform.tag == "Element")
                {
                    Element_Battle temp = hit.transform.GetComponent<Element_Battle>();
                    if (temp.GetState() != Element_Battle.Element_State.Disable)
                    {
                        drag_element = hit.transform.GetComponent<Element_Battle>();
                    }
                   break;
                }
            }
        }
        else if (drag_element)
        {
            Vector2 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            drag_element.transform.position = pos;
        }

        if (!Input.GetMouseButton(0))
        {
            if (drag_element)
            {
                drag_element.DropElement();
                drag_element = null;
            }
        }
    }

}
