﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PrisonerManager : MonoBehaviour
{
    GameObject targetimage;

    // Start is called before the first frame update
    void Start()
    {
        targetimage = transform.Find("TargetInfo").Find("TargetImage").gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void TouchedPrisoner(GameObject obj)
    {
        var oriImg = targetimage.GetComponent<Image>();
        var tarImg = obj.GetComponent<Image>();

        oriImg.sprite = tarImg.sprite;
    }
}
