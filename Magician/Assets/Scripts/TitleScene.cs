﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TitleScene : MonoBehaviour
{
    private void Awake() {
        Screen.SetResolution(720,1280,true);
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void FixedUpdate() {
        if(Input.GetMouseButtonUp(0))
        {
            Debug.Log("SceneLoad");
            SceneManager.LoadScene("MainScene", LoadSceneMode.Single);
        }
    }
}
