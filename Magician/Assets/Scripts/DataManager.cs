﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


using System;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

public class DataManager : MonoBehaviour
{
    List<Buff_Data> buffs;
    List<Element_Data> elements;
    List<Equipment_Data> equipments;
    List<EXP_Data> exps;
    List<Familiar_Data> familiars;
    List<Familiar_Book_Data> familiar_books;
    List<Familiar_Script_Data> familiar_Scripts;
    List<Force_Data> force;
    List<Item_Data> items;
    List<Item_Option_Data> item_options;
    List<Monster_Data> monsters;
    List<Prototype_Shop_Data> shops;
    List<Stage_Data> stages;
    List<String_Script_Data> scripts;

    public Prison_Familiar_Data[] PrisonerData;

    // Start is called before the first frame update

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);

        PrisonerData = LoadDataAsList<Prison_Familiar_Data>("Assets/Data/Prison_Familiar_Data.xml");
    }
   
    void LoadAll()
    {
        XMLLoader loader = new XMLLoader();

        buffs = loader.LoadBuff_Data("Assets/Data/Buff.xml");
        elements = loader.LoadElement_Data("Assets/Data/Element.xml");
        equipments = loader.LoadEquipment_Data("Assets/Data/Equipment_Data.xml");
        exps = loader.LoadEXP_Data("Assets/Data/EXP.xml");
        familiars = loader.LoadFamiliar_Data("Assets/Data/Familiar.xml");
        familiar_books = loader.LoadFamiliar_Book_Data("Assets/Data/Familiar_Book.xml");
        familiar_Scripts = loader.LoadFamiliar_Script_Data("Assets/Data/Familiar_Script.xml");
        force = loader.LoadForce_Data("Assets/Data/Force.xml");
        items = loader.LoadItem_Data("Assets/Data/Item.xml");
        item_options = loader.LoadItem_Option_Data("Assets/Data/Item_Option.xml");
        monsters = loader.LoadMonster_Data("Assets/Data/Monster.xml");
        shops = loader.LoadPrototype_Shop_Data("Assets/Data/Prototype_Shop.xml");
        stages = loader.LoadStage_Data("Assets/Data/Stage.xml");
        scripts = loader.LoadString_Script_Data("Assets/Data/String_Script.xml");

    }

    T[] LoadDataAsList<T>(string path)
    {
        XmlSerializer serializer = new XmlSerializer(typeof(T[]));
        StreamReader reader = new StreamReader(path);
        T[] Datas = serializer.Deserialize(reader.BaseStream) as T[];
        reader.Close();
        return Datas;
    }

    void SavePrisonerData()
    {
        List<Prison_Familiar_Data> D = new List<Prison_Familiar_Data>();
        for(int i=0;i<5;i++)
        {
            Prison_Familiar_Data temp = new Prison_Familiar_Data();
            temp.familiarID = i;
            temp.isGet = false;
            temp.now_like = i;
            temp.max_like = i;

            D.Add(temp);
        }

        XmlSerializer seri = new XmlSerializer(typeof(List<Prison_Familiar_Data>));
        StreamWriter writer = new StreamWriter("Assets/Data/Prison_Familiar_Data.xml");
        seri.Serialize(writer.BaseStream, D);
        writer.Close();
    }
}
