﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;


public class Atlas_Manager : MonoBehaviour
{
    SpriteAtlas Element_Atlas;
    // Start is called before the first frame update
    void Awake()
    {
        Element_Atlas = new SpriteAtlas();

        Element_Atlas = Resources.Load("Element/Element") as SpriteAtlas;

    }
    public Sprite[] GetElementSprite()
    {
        var result = new Sprite[Element_Atlas.spriteCount];
        result[0] = Element_Atlas.GetSprite("light1");
        result[1] = Element_Atlas.GetSprite("dark1");
        result[2] = Element_Atlas.GetSprite("fire1");
        result[3] = Element_Atlas.GetSprite("water1");

        result[4] = Element_Atlas.GetSprite("light3");
        result[5] = Element_Atlas.GetSprite("dark3");
        result[6] = Element_Atlas.GetSprite("fire3");
        result[7] = Element_Atlas.GetSprite("water3");

        return result;
    }
}
